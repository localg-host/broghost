========
BroGhost
========

Keep an eye on your servers like a brother

Quickstart
==========

You should install WatchGhost_ first, then:

.. code-block:: shell

  pip install broghost
  env FLASK_APP=broghost flask run
  $NAVIGATOR http://localhost:5000/

.. _WatchGhost: https://gitlab.com/localg-host/watchghost/


Code on broghost
================

.. code-block:: shell

  git clone https://gitlab.com/localg-host/broghost.git
  cd broghost
  pip install -e .
  env FLASK_DEBUG=1 FLASK_APP=broghost flask run
