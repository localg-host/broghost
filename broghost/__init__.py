# BroGhost, keep an eye on your servers like a brother
# Copyright © 2019 Guillaume Ayoub

from datetime import datetime, timedelta, timezone

from influxdb import InfluxDBClient
from flask import Flask, render_template, request
from math import ceil
import ast
import copy
import re
import os

app = Flask(__name__)
client = InfluxDBClient(
    host=os.getenv("INFLUXDB_HOST", 'localhost'),
    port=int(os.getenv("INFLUXDB_PORT", 8086)),
    username=os.getenv("INFLUXDB_USERNAME", 'root'),
    password=os.getenv("INFLUXDB_PASSWORD", 'root'),
    database=os.getenv("INFLUXDB_DATABASE", 'watchghost'),
)


def set_status(step_statuses, aggregate_statuses):
    for status in ('unknown', 'critical', 'error', 'warning', 'info'):
        if step_statuses.get(status):
            break
    else: # no break
        status = 'unknown'
    step_statuses['status'] = status
    if status == 'info':
        aggregate_statuses['info'] += 1
    if status != 'unknown':
        aggregate_statuses['total'] += 1


@app.route('/')
def index():
    original_tag = tag = request.args.get('tag', 'server')
    original_delta = delta = request.args.get('delta', '24h')
    original_step = step = request.args.get('step')
    original_start = start = request.args.get('start')
    original_stop = stop = request.args.get('stop')

    tag = f'tag.{tag}'

    if start and stop:
        start = datetime.strptime(start.split('.')[0], '%Y-%m-%dT%H:%M:%S')
        start = start.replace(tzinfo=timezone.utc)
        stop = datetime.strptime(stop.split('.')[0], '%Y-%m-%dT%H:%M:%S')
        stop = stop.replace(tzinfo=timezone.utc)
        delta = stop - start
        seconds = delta.total_seconds()
    else:
        seconds = 60 * 60
        if delta.endswith('d'):
            seconds *= 24
        seconds *= int(delta[:-1])
        delta = timedelta(seconds=seconds)
        stop = datetime.now(timezone.utc)
        start = stop - delta
    if seconds > 60 * 24 * 60 * 60:  # 60 days
        step = timedelta(days=1)
    elif seconds > 15 * 24 * 60 * 60:  # 15 days
        step = timedelta(hours=8)
    elif seconds > 4 * 24 * 60 * 60:  # 4 days
        step = timedelta(hours=2)
    elif seconds > 12 * 60 * 60:  # 12 hours
        step = timedelta(minutes=15)
    else:
        step = timedelta(minutes=5)

    if original_step:
        step = timedelta(seconds=int(original_step))

    query = client.query(
        'select count("status") '
        'from /.*/ '
        'where is_hard = true '
        'and time >= \'{}\' '
        'and time <= \'{}\' '
        'and "{}" != \'\' '
        'group by "tag.status", "{}", "tag.name", time({}s)'.format(
            start.isoformat(), stop.isoformat(), tag, tag,
            int(step.total_seconds())
        )
    )

    data = {}
    group_data = {}
    statuses = {}
    group_statuses = {}
    for (name, tags), values in query.items():
        if tags[tag] not in data:
            data[tags[tag]] = {}
            statuses[tags[tag]] = {'info': 0, 'total': 0}
            group_data[tags[tag]] = {}
            group_statuses[tags[tag]] = {}
        if tags['tag.name'] not in group_data[tags[tag]]:
            group_data[tags[tag]][tags['tag.name']] = {}
            group_statuses[tags[tag]][tags['tag.name']] = {
                'info': 0, 'total': 0}

        data_tag = data[tags[tag]]
        group_data_tag = group_data[tags[tag]][tags['tag.name']]

        for value in values:
            time_tmp = datetime.strptime(
                value['time'][:-1].split('.')[0], '%Y-%m-%dT%H:%M:%S')
            date = time_tmp.date().strftime('%B %d')
            if step.seconds:
                start = time_tmp.time().strftime('%H:%M')
                stop = (time_tmp + step).time().strftime('%H:%M')
                time = '{}\n{} → {} (UTC)'.format(date, start, stop)
            else:
                time = '{}'.format(date)

            if time not in data_tag:
                data_tag[time] = {tags['tag.status']: value['count'], 'start' : time_tmp, 'stop'  : time_tmp + step}
            elif tags['tag.status'] in data_tag[time]:
                data_tag[time][tags['tag.status']] += value['count']
            else:
                data_tag[time][tags['tag.status']] = value['count']

            if time not in group_data_tag:
                group_data_tag[time] = {tags['tag.status']: value['count'], 'start' :time_tmp, 'stop' :time_tmp + step}
            elif tags['tag.status'] in group_data_tag[time]:
                group_data_tag[time][tags['tag.status']] += value['count']
            else:
                group_data_tag[time][tags['tag.status']] = value['count']

    for group, data_tag in data.items():
        for values in data_tag.values():
            set_status(values, statuses[group])
        for name, group_data_tag in group_data[group].items():
            for values in group_data_tag.values():
                set_status(values, group_statuses[group][name])

    uptimes = {
        key: ((100 * value['info'] / value['total'])
              if value['total'] else None)
        for key, value in statuses.items()
    }
    group_uptimes = {
        group_key: {
            key: ((100 * value['info'] / value['total'])
                  if value['total'] else None)
            for key, value in statuses.items()
        } for group_key, statuses in group_statuses.items()
    }

    query = client.query('show tag keys')
    tags_list = {}
    for item, tags in query.items():
        for tag in tags:
            if tag['tagKey'].startswith('tag.'):
                tag_name = tag['tagKey'][4:]
                if tag_name not in ('name', 'status'):
                    tags_list[tag_name.title()] = tag_name

    tag_names = {value: key for key, value in tags_list.items()}
    return render_template(
        'index.jinja2.html',
        title='Availability - {}'.format(tag_names[original_tag]),
        data=data, group_data=group_data, tags=tags_list,
        original_tag=original_tag, original_step=original_step,
        original_start=original_start, original_stop=original_stop,
        original_delta=original_delta, uptimes=uptimes,
        group_uptimes=group_uptimes,
    )


def unflatten(dictionary):
    result_dict = {}
    for key, value in dictionary.items():
        if value :
            ## Watchghost logger is sending bytes casted as string.
            ## It is not possible to convert them back so we format them a bit better with regex
            if type(value) == str and re.search(r"^b[\"\'].*[\'\"]$", value)  :
                value = '<br>' + value[2:-1].replace('\\n', '<br>')
            parts = key.split(".")
            d = result_dict
            for part in parts[:-1]:
                if part not in d:
                    d[part] = dict()
                d = d[part]
            d[parts[-1]] = value
    return result_dict


def update_pagination(pagination, name, page) :
    new_pagination = copy.deepcopy(pagination)
    new_pagination[name]['page'] = page
    return new_pagination

def get_pagination_range(pagination, name) :
    cur_page = pagination[name]['page']
    min_page = cur_page - 2 if cur_page > 3 else 1
    max_page = cur_page + 2 if cur_page + 2 < pagination[name]['page_nb'] else pagination[name]['page_nb']
    rg = list(range(min_page, max_page +1))
    if min_page != 1 :
        rg = [1] + rg
    if max_page != pagination[name]['page_nb'] :
        rg = rg + [pagination[name]['page_nb']]
    return rg

@app.route('/detail')
def detail() :
    original_tag = tag = request.args.get('tag')
    group = request.args.get('group')
    subname = request.args.get('name')
    original_start = start = request.args.get('start')
    original_stop = stop = request.args.get('stop')
    time =  request.args.get('time')
    status = request.args.get('status')
    pagination = ast.literal_eval(request.args.get('pagination', '{}'))
    back_url = request.args.get('back_url')

    if not re.match(r'^\w+$', tag) :
        raise ValueError("tag parameter should only have word character")

    tag = f'tag.{tag}'

    start = datetime.strptime(start.split('.')[0], '%Y-%m-%d %H:%M:%S')
    start = start.replace(tzinfo=timezone.utc)
    stop = datetime.strptime(stop.split('.')[0], '%Y-%m-%d %H:%M:%S')
    stop = stop.replace(tzinfo=timezone.utc)


    params = {
        "start" : start.isoformat(),
        "stop" : stop.isoformat(),
        "group" : group,
        "subname" : subname,
        "status" : status
    }
    query = f'select count("start") from /.*/ where is_hard = true and time >= $start and time <= $stop and "{tag}" = $group '
    query += 'and "tag.name" = $subname ' if subname else ''
    query += 'and "tag.status" = $status '     if status else ''
    measurments = client.query (query, bind_params = params)

    page_max = 10
    data={}
    for (measurement_name, m_tags), measurement_counts in measurments.items() :
        if measurement_name not in pagination.keys() :
            pagination[measurement_name] = {'page' : 1}

        for count in measurement_counts :
            pagination[measurement_name]['page_nb'] = ceil(count['count'] / page_max)

        page = int(pagination[measurement_name]['page'])
        
        query_str = f'select * from "{measurement_name}" '
        query_str +=  f'where is_hard = true and time >= $start and time <= $stop and  "{tag}" = $group '
        query_str += 'and "tag.name" = $subname ' if subname else ''
        query_str += 'and "tag.status" = $status ' if status else ''
        query_str += f'ORDER BY time DESC limit {page_max} OFFSET {(page-1)*page_max}'
        query = client.query(query_str, bind_params = params)

        for (name, tags), logs in query.items():
            if name not in data.keys() :
                data[name] = []
            for log in logs :
                data[name].append(unflatten(log))


    return render_template(
        'detail.jinja2.html',
        title='{}{} - {} - {}'.format(group, ('/'+subname) if subname else '', status.title(), time),
        data=data,
        pagination=pagination,
        tag = original_tag,
        group = group,
        subname = subname ,
        start = original_start ,
        stop = original_stop ,
        time = time ,
        status = status,
        update_pagination = update_pagination,
        get_pagination_range = get_pagination_range,
        back_url = back_url,
    )
